FROM openjdk:11
ADD target/projeto-consumidor.jar projeto-consumidor.jar
ENTRYPOINT ["java", "-jar","projeto-consumidor.jar"]
EXPOSE 8081
