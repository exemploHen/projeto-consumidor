create sequence seq_id_external_request;

create table external_request
(
    id numeric default nextval('seq_id_external_request'::regclass) not null
        constraint pk_external_request_id primary key,
    url varchar not null,
    response varchar not null
);
