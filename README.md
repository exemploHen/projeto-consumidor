# Configuração da aplicação
### O que fazer para rodar a aplicação:

1. Clone o projeto para sua máquina local [Repositório Git](https://gitlab.com/exemploHen/projeto-consumidor.git)
    - git clone git@gitlab.com:exemploHen/projeto-consumidor.git
3. Construa a aplicação utilizando o [Maven](https://maven.apache.org/) no diretório do arquivo pom.xml
    - mvn clean package -DskipTests=true
4. Execute o ambiente [Docker](https://www.docker.com/) no mesmo diretório que o arquivo docker-compose.yml o comando:
    - docker-compose up --build
5. Pronto!!! A aplicação e o banco de dados já estão rodando!!!!!

### Acesso a aplicação
Para acessar a aplicação, via [Postman](https://www.postman.com/) ou página:

* Documentação pelo Postman, importação pelo [link](https://www.getpostman.com/collections/02bca5c741288fae27f0)
