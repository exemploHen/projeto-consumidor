package br.com.consumidor.projetoconsumidor.service;

import br.com.consumidor.projetoconsumidor.model.ExternalRequestEntity;
import br.com.consumidor.projetoconsumidor.repository.ExternalRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExternalRequestService {

  private final ExternalRequestRepository repository;

  @Autowired
  public ExternalRequestService(ExternalRequestRepository repository) {
    this.repository = repository;
  }

  public void saveNewEntity(String url, String response) {
    repository.save(new ExternalRequestEntity(url, response));
  }
}
