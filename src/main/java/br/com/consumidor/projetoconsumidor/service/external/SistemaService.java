package br.com.consumidor.projetoconsumidor.service.external;

import br.com.consumidor.projetoconsumidor.exception.ComunicacaoException;
import br.com.consumidor.projetoconsumidor.service.ExternalRequestService;
import br.com.consumidor.projetoconsumidor.service.external.dto.request.SolicitacaoAtendimentoRequestDTO;
import br.com.consumidor.projetoconsumidor.service.external.dto.response.RealizarAtendimentoResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SistemaService {

  private final RestTemplate restTemplate;
  private final ExternalRequestService entityService;
  private final static String urlExterna = "http://localhost:8080/sistema-externo/atendimento";

  @Autowired
  public SistemaService(RestTemplate restTemplate, ExternalRequestService entityService) {
    this.restTemplate = restTemplate;
    this.entityService = entityService;
  }

  public RealizarAtendimentoResponseDTO consumirSistemaExterno(String parametro, Integer numero)
      throws ComunicacaoException {
    SolicitacaoAtendimentoRequestDTO requestSistemaExterno =
        new SolicitacaoAtendimentoRequestDTO(parametro, numero);

    try {
      ResponseEntity<RealizarAtendimentoResponseDTO> responseSistemaExterno = restTemplate.postForEntity(
          urlExterna, requestSistemaExterno, RealizarAtendimentoResponseDTO.class);

      RealizarAtendimentoResponseDTO body = responseSistemaExterno.getBody();
      entityService.saveNewEntity(urlExterna, body.toString());

      return body;
    } catch (Exception e) {
      throw new ComunicacaoException(e.getMessage());
    }
  }

}
