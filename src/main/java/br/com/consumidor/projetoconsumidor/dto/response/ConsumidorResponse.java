package br.com.consumidor.projetoconsumidor.dto.response;

public class ConsumidorResponse {

  private String mensagem;
  private Integer codigo;

  public ConsumidorResponse(String mensagem, Integer codigo) {
    this.mensagem = mensagem;
    this.codigo = codigo;
  }

  public ConsumidorResponse() {
  }

  public String getMensagem() {
    return mensagem;
  }

  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }

  public Integer getCodigo() {
    return codigo;
  }

  public void setCodigo(Integer codigo) {
    this.codigo = codigo;
  }
}
