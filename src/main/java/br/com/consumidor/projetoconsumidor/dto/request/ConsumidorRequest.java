package br.com.consumidor.projetoconsumidor.dto.request;

public class ConsumidorRequest {

  private String parametro;
  private Integer numero;

  public ConsumidorRequest() {
  }

  public ConsumidorRequest(String parametro, Integer numero) {
    this.parametro = parametro;
    this.numero = numero;
  }

  public String getParametro() {
    return parametro;
  }

  public void setParametro(String parametro) {
    this.parametro = parametro;
  }

  public Integer getNumero() {
    return numero;
  }

  public void setNumero(Integer numero) {
    this.numero = numero;
  }
}
