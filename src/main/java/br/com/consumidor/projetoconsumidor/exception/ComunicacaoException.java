package br.com.consumidor.projetoconsumidor.exception;

public class ComunicacaoException extends Exception {

  public ComunicacaoException(String message) {
    super(message);
  }
}
