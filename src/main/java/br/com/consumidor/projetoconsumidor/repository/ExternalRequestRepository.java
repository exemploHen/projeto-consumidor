package br.com.consumidor.projetoconsumidor.repository;

import br.com.consumidor.projetoconsumidor.model.ExternalRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExternalRequestRepository extends JpaRepository<ExternalRequestEntity, Long> {

}
