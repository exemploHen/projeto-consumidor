package br.com.consumidor.projetoconsumidor.controller;

import br.com.consumidor.projetoconsumidor.dto.request.ConsumidorRequest;
import br.com.consumidor.projetoconsumidor.dto.response.ConsumidorResponse;
import br.com.consumidor.projetoconsumidor.exception.ComunicacaoException;
import br.com.consumidor.projetoconsumidor.service.external.SistemaService;
import br.com.consumidor.projetoconsumidor.service.external.dto.response.RealizarAtendimentoResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class ConsumidorController {

  private final SistemaService sistemaService;

  @Autowired
  public ConsumidorController(SistemaService sistemaService) {
    this.sistemaService = sistemaService;
  }

  @PostMapping("realizarProcessamento")
  public ResponseEntity<ConsumidorResponse> realizarAtendimentoExterno(
      @RequestBody ConsumidorRequest request) {
    ConsumidorResponse response;
    try {
      RealizarAtendimentoResponseDTO atendimento = sistemaService.consumirSistemaExterno(
          request.getParametro(), request.getNumero());
      response = new ConsumidorResponse(atendimento.getMensagem(), atendimento.getCode());
      return ResponseEntity.ok(response);
    } catch (ComunicacaoException e) {
      response = new ConsumidorResponse(e.getMessage(), 500);
      return ResponseEntity.internalServerError().body(response);
    }
  }



}
